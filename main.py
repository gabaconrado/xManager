'''
    file: main.py
    author: gabriel conrado
    project: xManager
    brief:
        Entry point for xManager system
'''

from xm.functions import login, parse_inputs, build_command_dictionary


def main():
    '''
    Main function
    Contains the main loop of the app
    @params: none
    @return: none
    '''
    logged, user = login()
    if not logged:
        print("Login failed")
        return
    build_command_dictionary()
    print('Welcome {}, to leave use "exit" command.'.format(user))
    while True:
        cmd = input("\n> ").lower()
        if cmd == 'exit':
            break
        parse_inputs(cmd)
    print("Bye!")


if __name__ == "__main__":
    main()
