# xManager

Hello, this is a simple command line money manager!

This project was made with the objective of learning python, it is licensed
by the apache license so feel free to use it as you wish.

Also, if you find a bug feel free to create an issue, and if you want to 
improve it somehow collabs are very welcome!

# Install

The latest release is the version 0.00 and can be downloaded [here](https://gitlab.com/gabaconrado/xManager/raw/master/deploy/releases/xmanager-v0-0-0.zip?inline=false).

Just run the entrypoint:

```bash
python main.py
```

# Contribute

To contribute, clone the repo and install the linters in `deploy/requirements.txt` with pip.

After every commit, validate the new code running the test scripts:

```bash
# do not forget to give the execute permission(Only once)
sudo chmod 700 test_commit.sh

./test_commit.sh
```

# Documentation

The software is more detailed in the project's [wiki](https://gitlab.com/gabaconrado/xManager/wikis/home)