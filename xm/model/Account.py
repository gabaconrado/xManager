'''
    file: Account.py
    author: gabriel conrado
    project: xManager
    brief:
        Model class for the entity Account
'''

from abc import abstractmethod

from xm.database import insert_account, get_account, TYPES_ACCOUNT, \
     delete_account, update_account
from xm.model.User import User
from xm.exceptions import ValidationException


class Account:
    '''
    Account object abstract representation
    '''

    @staticmethod
    def get(**kwargs):
        '''
        Get accounts from db
        params: account parameters
        return: list of accounts
        @raises: DatabaseException if SQL Operational error
        '''
        account_data = get_account(**kwargs)
        if account_data:
            accounts = []
            for ad in account_data:
                owner = User.get(id=ad['owner']['id'])[0]
                if ad['type'] == TYPES_ACCOUNT["current"]:
                    account = Current(
                        alias=ad['alias'],
                        owner=owner,
                        description=ad['description'],
                        balance=ad['balance'],
                        account_id=ad['id']
                        )
                else:
                    account = Card(
                        alias=ad['alias'],
                        owner=owner,
                        description=ad['description'],
                        balance=ad['balance'],
                        max_limit=ad['max_limit'],
                        due_date=ad['due_date'],
                        account_id=ad['id']
                        )
                accounts.append(account)
            return accounts
        return None

    def __init__(self, alias, owner, type_account,
                 description, balance=0, account_id=None):
        '''
        Initialization method for object
        @params: account parameters
        '''
        self.id = account_id
        self.alias = alias
        self.owner = owner
        self.type = type_account
        self.description = description
        self.balance = balance

    def __str__(self):
        '''
        Human readable representation of object
        @params: itself
        @return: Account alias
        '''
        return self.alias

    def _clean(self):
        '''
        Data validation for the account model
        @params: the account
        @return: None
        @raises: ValidationException if some field is inconsistent
        '''
        if self.id is not None:
            if not isinstance(self.id, int):
                raise ValidationException(
                    message='Account ID must be an int',
                    eclass='Account',
                    field='ID'
                    )
            if self.id < 1:
                raise ValidationException(
                    message='Account ID cannot be less than 1',
                    eclass='Account',
                    field='ID'
                    )
        if not isinstance(self.alias, str):
            raise ValidationException(
                message='Alias must be a string',
                eclass='Account',
                field='Alias'
                )
        if self.description is not None:
            if not isinstance(self.description, str):
                raise ValidationException(
                    message='Description must be a string',
                    eclass='Account',
                    field='Description'
                    )
        if self.type not in [TYPES_ACCOUNT['current'], TYPES_ACCOUNT['card']]:
            raise ValidationException(
                message='Type account must be Current or Card',
                eclass='Account',
                field='Type account'
                )
        if not isinstance(self.owner, User):
            raise ValidationException(
                message='Invalid owner parameter(Wrong type)',
                eclass='Account',
                field='Owner'
                )
        if not isinstance(self.balance, float):
            raise ValidationException(
                message='Balance must be a float',
                eclass='Account',
                field='Balance'
                )

    def save(self):
        '''
        Saves the account to DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        @raises: ValidationException if error validating fields
        '''
        self._clean()
        if self.id is None:
            account_data = self.to_dict()
            insert_account(account_data=account_data)
            self.id = account_data['id']
        else:
            update_account(account_data=self.to_dict())

    def delete(self):
        '''
        Delete the account from DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        '''
        delete_account(account_id=self.id)

    def deposit(self, amount):
        '''
        Deposit an amount into the account
        @parameters: Amount of money
        '''
        self.balance += amount
        self.save()

    def withdral(self, amount):
        '''
        Withdraw an amount from the account
        @parameters: Amount of money
        @raises: ValidationException if balance drops to less than zero
        '''
        self.balance -= amount
        self.save()

    @abstractmethod
    def to_dict(self):
        '''
        Transform account object into a dictionary
        params: itself
        return: dictionary with data
        '''


class Current(Account):
    '''
    Current account object representation
    '''
    def __init__(self, alias, owner, description, balance, account_id=None):
        super(Current, self).__init__(
            alias,
            owner,
            TYPES_ACCOUNT["current"],
            description,
            balance,
            account_id
            )

    def to_dict(self):
        '''
        Transform account object into a dictionary
        params: itself
        return: dictionary with data
        '''
        return {'id': self.id, 'alias': self.alias,
                'owner': self.owner.to_dict(),
                'description': self.description,
                'balance': self.balance,
                'type': self.type}


class Card(Account):
    '''
    Card account object representation
    '''

    def __init__(self, alias, owner, description,
                 max_limit, due_date, balance=0, account_id=None):
        super(Card, self).__init__(
            alias,
            owner,
            TYPES_ACCOUNT['card'],
            description,
            balance,
            account_id
            )
        self.max_limit = max_limit
        self.due_date = due_date

    def expense(self, value):
        '''
        Pay an expense with the card
        @param float value: the value of the expense
        @return: none
        '''

    def pay_invoice(self, amount=None):
        '''
        Pay the invoice and reset the limit
        @parameters: none
        @return: none
        '''

    def to_dict(self):
        '''
        Transform account object into a dictionary
        params: itself
        return: dictionary with data
        '''
        return {'id': self.id, 'alias': self.alias,
                'owner': self.owner.to_dict(),
                'description': self.description, 'max_limit': self.max_limit,
                'due_date': self.due_date, 'type': self.type,
                'balance': self.balance}

    def _clean(self):
        '''
        Data validation for the account model
        @params: itself
        @return: None
        @raises: ValidationException if some field is inconsistent
        '''
        super(Card, self)._clean()
        if not isinstance(self.max_limit, float):
            raise ValidationException(
                message='Limit must be a float',
                eclass='Account',
                field='Limit'
                )
        if self.max_limit < 0:
            raise ValidationException(
                message='Limit must be greater than 0',
                eclass='Account',
                field='Limit'
                )
        if not isinstance(self.due_date, int):
            raise ValidationException(
                message='Due date must be an int',
                eclass='Account',
                field='Due date'
                )
        if self.due_date < 0 or self.due_date > 31:
            raise ValidationException(
                message='Due date must be between 0 and 31',
                eclass='Account',
                field='Due date'
                )
