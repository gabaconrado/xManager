'''
    file: User.py
    author: gabriel conrado
    project: xManager
    brief:
        Model class for the entity User
'''

from xm.database import insert_user, get_user, delete_user, \
                        update_user

from xm.exceptions import ValidationException


class User:
    '''
    User object representation
    '''

    @staticmethod
    def get(**kwargs):
        '''
        Get users from db
        @params: user parameters
        @return: list of users
        @raises: DatabaseException if SQL Operational error
        '''
        user_data = get_user(**kwargs)
        if user_data:
            users = []
            for ud in user_data:
                users.append(User(id_user=ud['id'], name=ud['name'],
                                  password=ud['password']))
            return users
        return None

    def __init__(self, name, password, id_user=None):
        '''
        Initialization method for object
        @params: user parameters
        @raises: DatabaseException if SQL Operational error
        '''
        self.id = id_user
        self.name = name
        self.password = password

    def __str__(self):
        '''
        Human readable representation of object
        @params: itself
        @return: string representation
        '''
        return self.name

    def _clean(self):
        '''
        Data validation for the user model
        @params: the user
        @return: None
        @raises: ValidationException if some field is inconsistent
        '''
        if self.id is not None:
            if not isinstance(self.id, int):
                raise ValidationException(
                    message='User ID must be an int',
                    eclass='User',
                    field='ID'
                    )
            if self.id < 1:
                raise ValidationException(
                    message='User ID cannot be less than 1',
                    eclass='User',
                    field='ID'
                    )
        if not isinstance(self.name, str):
            raise ValidationException(
                message='Name must be a string',
                eclass='User',
                field='Name')
        if not isinstance(self.password, str):
            raise ValidationException(
                message='Password must be a string',
                eclass='User',
                field='Password'
                )

    def save(self):
        '''
        Saves the user to DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        @raises: ValidationException if error validating fields
        '''
        self._clean()
        if self.id is None:
            user_data = self.to_dict()
            insert_user(user_data=user_data)
            self.id = user_data['id']
        else:
            update_user(user_data=self.to_dict())

    def delete(self):
        '''
        Delete the user from DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        '''
        delete_user(user_id=self.id)

    def to_dict(self):
        '''
        Transform user object into a dictionary
        @params: itself
        @return: dictionary with data
        '''
        return {'id': self.id, 'name': self.name, 'password': self.password}
