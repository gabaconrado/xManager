'''
    file: Operation.py
    author: gabriel conrado
    project: xManager
    brief:
        Model class for the entity Operation
'''

import datetime

from xm.database import get_operation, insert_operation, \
                                    delete_operation, update_operation
from xm.model.Account import Account, Current, Card
from xm.exceptions import ValidationException

ACCOUNT_TYPES = [Card, Current]


class Operation:
    '''
    Operation object description
    '''

    @staticmethod
    def get(**kwargs):
        '''
        Get operations from db
        params: operation parameters
        return: list of operations
        @raises: DatabaseException if SQL Operational error
        '''
        operation_data = get_operation(**kwargs)
        if operation_data:
            operations = []
            for od in operation_data:
                source = Account.get(id=od['source']['id'])[0]
                destiny = Account.get(id=od['destiny']['id'])[0]
                operation = Operation(
                    operation_id=od['id'],
                    source=source,
                    destiny=destiny,
                    amount=od['amount'],
                    date=od['date'],
                    description=od['description'])
                operations.append(operation)
            return operations
        return None

    def __init__(self, source, destiny, amount, date,
                 description, operation_id=None):
        self.id = operation_id
        self.source = source
        self.destiny = destiny
        self.amount = amount
        self.date = date
        self.description = description

    def __str__(self):
        return self.description

    # pylint: disable=unidiomatic-typecheck
    def _clean(self):
        '''
        Data validation for the operation model
        @params: the operation
        @return: None
        @raises: ValidationException if some field is inconsistent
        '''
        if self.id is not None:
            if not isinstance(self.id, int):
                raise ValidationException(
                    message='Operation ID must be an int',
                    eclass='Operation',
                    field='ID'
                    )
            if self.id < 1:
                raise ValidationException(
                    message='Operation ID cannot be less than 1',
                    eclass='Operation',
                    field='ID'
                    )
        if type(self.source) not in ACCOUNT_TYPES:
            raise ValidationException(
                message='Invalid source(Wrong type "{}")'.format(
                    type(self.source)
                ),
                eclass='Operation',
                field='Source'
                )
        if type(self.destiny) not in ACCOUNT_TYPES:
            raise ValidationException(
                message='Invalid destiny(Wrong type "{}")'.format(
                    type(self.destiny)
                ),
                eclass='Operation',
                field='Destiny'
                )
        if not isinstance(self.amount, float):
            raise ValidationException(
                message='Amount must be a float',
                eclass='Operation',
                field='Amount'
                )
        if self.amount < 0:
            raise ValidationException(
                message='Amount must be greater than 0',
                eclass='Operation',
                field='Amount'
                )
        if not isinstance(self.date, datetime.datetime):
            raise ValidationException(
                message='Date must be a datetime',
                eclass='Operation',
                field='Date'
                )
        if not isinstance(self.description, str):
            raise ValidationException(
                message='Description must be a string',
                eclass='Operation',
                field='Description'
                )

    def save(self):
        '''
        Saves the operation to DB
        params: itself
        return: None
        @raises: DatabaseException if SQL Operational error
        @raises: ValidationException if error validating fields
        '''
        self._clean()
        if self.id is None:
            operation_data = self.to_dict()
            insert_operation(operation_data=operation_data)
            self.id = operation_data['id']
        else:
            old_operation = Operation.get(id=self.id)[0]
            old_operation.source.deposit(old_operation.amount)
            old_operation.destiny.withdral(old_operation.amount)
            self.source = Account.get(id=self.source.id)[0]
            self.destiny = Account.get(id=self.destiny.id)[0]
            update_operation(operation_data=self.to_dict())
        self.source.withdral(self.amount)
        self.destiny.deposit(self.amount)

    def delete(self):
        '''
        Delete the operation from DB
        params: itself
        return: None
        @raises: DatabaseException if SQL Operational error
        '''
        delete_operation(operation_id=self.id)
        self.source.deposit(self.amount)
        self.destiny.withdral(self.amount)

    def to_dict(self):
        '''
        Transform operation object into a dictionary
        params: itself
        return: dictionary with data
        '''
        return {'id': self.id, 'source': self.source.to_dict(),
                'destiny': self.destiny.to_dict(),
                'amount': self.amount, 'date': self.date,
                'description': self.description}
