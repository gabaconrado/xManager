'''
    file: Dream.py
    author: gabriel conrado
    project: xManager
    brief:
        Model class for the entity Dream
'''

import datetime

from xm.database import insert_dream, get_dream, delete_dream, \
                        update_dream

from xm.exceptions import ValidationException


class Dream:
    '''
    Dream object representation
    '''

    @staticmethod
    def get(**kwargs):
        '''
        Get dreams from db
        @params: dream parameters
        @return: list of dreams
        @raises: DatabaseException if SQL Operational error
        '''
        dream_data = get_dream(**kwargs)
        if dream_data:
            dreams = []
            for dd in dream_data:
                dreams.append(
                    Dream(
                        id_dream=dd['id'],
                        alias=dd['alias'],
                        achieved=dd['achieved'],
                        goal=dd['goal'],
                        date=dd['date']))
            return dreams
        return None

    def __init__(self, alias, achieved, goal, date, id_dream=None):
        '''
        Initialization method for object
        @params: dream parameters
        @raises: DatabaseException if SQL Operational error
        '''
        self.id = id_dream
        self.alias = alias
        self.achieved = achieved
        self.goal = goal
        self.date = date

    def __str__(self):
        '''
        Human readable representation of object
        @params: itself
        @return: string representation
        '''
        return self.alias

    def _clean(self):
        '''
        Data validation for the dream model
        @params: the dream
        @return: None
        @raises: ValidationException if some field is inconsistent
        '''
        if self.id is not None:
            if not isinstance(self.id, int):
                raise ValidationException(
                    message='Dream ID must be an int',
                    eclass='Dream',
                    field='ID'
                    )
            if self.id < 1:
                raise ValidationException(
                    message='Dream ID cannot be less than 1',
                    eclass='Dream',
                    field='ID'
                    )
        if not isinstance(self.alias, str):
            raise ValidationException(
                message='Name must be a string',
                eclass='Dream',
                field='Alias')
        if not isinstance(self.achieved, float):
            raise ValidationException(
                message='Achieved must be a float',
                eclass='Dream',
                field='Achieved'
                )
        if self.achieved < 0:
            raise ValidationException(
                message='Achieved must be greater than 0',
                eclass='Dream',
                field='Achieved'
                )
        if not isinstance(self.goal, float):
            raise ValidationException(
                message='Goal must be a float',
                eclass='Dream',
                field='Goal'
                )
        if self.goal < 0:
            raise ValidationException(
                message='Goal must be greater than 0',
                eclass='Dream',
                field='Goal'
                )
        if not isinstance(self.date, datetime.datetime):
            raise ValidationException(
                message='Date must be a datetime',
                eclass='Dream',
                field='Date'
                )

    def save(self):
        '''
        Saves the dream to DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        @raises: ValidationException if error validating fields
        '''
        self._clean()
        if self.id is None:
            dream_data = self.to_dict()
            insert_dream(dream_data=dream_data)
            self.id = dream_data['id']
        else:
            update_dream(dream_data=self.to_dict())

    def delete(self):
        '''
        Delete the dream from DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        '''
        delete_dream(dream_id=self.id)

    def to_dict(self):
        '''
        Transform user object into a dictionary
        @params: itself
        @return: dictionary with data
        '''
        return {'id': self.id, 'alias': self.alias, 'achieved': self.achieved,
                'goal': self.goal, 'date': self.date}
