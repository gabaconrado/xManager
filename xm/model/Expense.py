'''
    file: Expense.py
    author: gabriel conrado
    project: xManager
    brief:
        Model class for the entity Expense
'''

import datetime

from xm.database import get_expense, insert_expense, \
                        delete_expense, update_expense
from xm.model.Category import Category
from xm.model.Account import Account, Card, Current
from xm.exceptions import ValidationException

ACCOUNT_TYPES = [Card, Current]


class Expense:
    '''
    Expense object description
    '''

    @staticmethod
    def get(**kwargs):
        '''
        Get expenses from db
        params: expense parameters
        return: list of expenses
        @raises: DatabaseException if SQL operational error
        '''
        expense_data = get_expense(**kwargs)
        if expense_data:
            expenses = []
            for ed in expense_data:
                category = Category.get(id=ed['category']['id'])[0]
                account = Account.get(id=ed['account']['id'])[0]
                expense = Expense(
                    expense_id=ed['id'],
                    alias=ed['alias'],
                    value=ed['value'],
                    date=ed['date'],
                    account=account,
                    category=category)
                expenses.append(expense)
            return expenses
        return None

    def __init__(self, alias, value, date, account,
                 category, expense_id=None):
        self.id = expense_id
        self.alias = alias
        self.value = value
        self.date = date
        self.account = account
        self.category = category

    def __str__(self):
        return self.alias

    # pylint: disable=unidiomatic-typecheck
    def _clean(self):
        '''
        Data validation for the expense model
        @params: the expense
        @return: None
        @raises: ValidationException if some field is inconsistent
        '''
        if self.id is not None:
            if not isinstance(self.id, int):
                raise ValidationException(
                    message='Expense ID must be an int',
                    eclass='Expense',
                    field='ID'
                    )
            if self.id < 1:
                raise ValidationException(
                    message='Expense ID cannot be less than 1',
                    eclass='Expense',
                    field='ID'
                    )
        if type(self.account) not in ACCOUNT_TYPES:
            raise ValidationException(
                message='Invalid account(Wrong type "{}")'.format(
                    type(self.account)
                ),
                eclass='Expense',
                field='Account'
                )
        if not isinstance(self.category, Category):
            raise ValidationException(
                message='Invalid category(Wrong type "{}")'.format(
                    type(self.category)
                ),
                eclass='Expense',
                field='Category'
                )
        if not isinstance(self.value, float):
            raise ValidationException(
                message='Value must be a float',
                eclass='Expense',
                field='Value'
                )
        if not isinstance(self.date, datetime.datetime):
            raise ValidationException(
                message='Date must be a datetime',
                eclass='Expense',
                field='Date'
                )
        if not isinstance(self.alias, str):
            raise ValidationException(
                message='Alias must be a string',
                eclass='Expense',
                field='Alias'
                )

    def save(self):
        '''
        Saves the expense to DB
        params: itself
        return: None
        @raises: DatabaseException if SQL Operational error
        @raises: ValidationException if error validating fields
        '''
        self._clean()
        if self.id is None:
            expense_data = self.to_dict()
            insert_expense(expense_data=expense_data)
            self.id = expense_data['id']
        else:
            old_expense = Expense.get(id=self.id)[0]
            old_expense.account.deposit(old_expense.value)
            self.account = Account.get(id=self.account.id)[0]
            update_expense(expense_data=self.to_dict())
        self.account.withdral(self.value)

    def delete(self):
        '''
        Delete the expense from DB
        params: itself
        return: None
        @raises: DatabaseException if SQL Operational error
        '''
        delete_expense(expense_id=self.id)
        self.account.deposit(self.value)

    def to_dict(self):
        '''
        Transform expense object into a dictionary
        params: itself
        return: dictionary with data
        '''
        return {'id': self.id, 'category': self.category.to_dict(),
                'account': self.account.to_dict(),
                'value': self.value, 'date': self.date,
                'alias': self.alias}
