'''
    file: Category.py
    author: gabriel conrado
    project: xManager
    brief:
        Model class for the entity Category
'''

from xm.database import insert_category, get_category, delete_category, \
                        update_category

from xm.exceptions import ValidationException


class Category:
    '''
    Category object representation
    '''

    @staticmethod
    def get(**kwargs):
        '''
        Get categories from db
        @params: category parameters
        @return: list of categories
        @raises: DatabaseException if SQL Operational error
        '''
        category_data = get_category(**kwargs)
        if category_data:
            categories = []
            for cd in category_data:
                categories.append(Category(id_category=cd['id'],
                                           alias=cd['alias']))
            return categories
        return None

    def __init__(self, alias, id_category=None):
        '''
        Initialization method for object
        @params: category parameters
        @raises: DatabaseException if SQL Operational error
        '''
        self.id = id_category
        self.alias = alias

    def __str__(self):
        '''
        Human readable representation of object
        @params: itself
        @return: string representation
        '''
        return self.alias

    def _clean(self):
        '''
        Data validation for the category model
        @params: the category
        @return: None
        @raises: ValidationException if some field is inconsistent
        '''
        if self.id is not None:
            if not isinstance(self.id, int):
                raise ValidationException(
                    message='Category ID must be an int',
                    eclass='Category',
                    field='ID'
                    )
            if self.id < 1:
                raise ValidationException(
                    message='Category ID cannot be less than 1',
                    eclass='Category',
                    field='ID'
                    )
        if not isinstance(self.alias, str):
            raise ValidationException(
                message='Alias must be a string',
                eclass='Category',
                field='Alias')

    def save(self):
        '''
        Saves the category to DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        @raises: ValidationException if error validating fields
        '''
        self._clean()
        if self.id is None:
            category_data = self.to_dict()
            insert_category(category_data=category_data)
            self.id = category_data['id']
        else:
            update_category(category_data=self.to_dict())

    def delete(self):
        '''
        Delete the category from DB
        @params: itself
        @raises: DatabaseException if SQL Operational error
        '''
        delete_category(category_id=self.id)

    def to_dict(self):
        '''
        Transform category object into a dictionary
        @params: itself
        @return: dictionary with data
        '''
        return {'id': self.id, 'alias': self.alias}
