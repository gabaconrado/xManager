'''
    file: exceptions.py
    author: gabriel conrado
    project: xManager
    brief:
        Exception definitions for the system
'''


class DatabaseException(Exception):
    '''
    Exception to be thrown in case of a database error
    :attributes
        message: the error message
        table: the table where the error ocurred
        operation: the tried operation
    '''

    def __init__(self, etype, operation, table, message):
        '''
        Initialization of the exception
        @param: str message: the error message
        @param: str table: the table where the error ocurred
        @param: str operation: the tried operation
        '''
        super(DatabaseException, self).__init__()
        self.etype = etype
        self.operation = operation
        self.table = table
        self.message = message

    def __str__(self):
        '''
        Human readable representation for the exception
        '''
        return '{} during {} at table "{}".\n{}'.format(self.etype,
                                                        self.operation,
                                                        self.table,
                                                        self.message)


class ValidationException(Exception):
    '''
    Exception to be thrown in case of model validation errors
    :attributes
        message: error message
        class: the class where error ocurred
        field: the field where error ocurred
    '''

    def __init__(self, message, eclass, field):
        '''
        Initialization of the exception
        @param: str message: error message
        @param: str class: class where error ocurred
        @param: str field: field where error ocurred
        '''
        super(ValidationException, self).__init__()
        self.message = message
        self.eclass = eclass
        self.field = field

    def __str__(self):
        '''
        Human readable representation for the exception
        '''
        return 'Error validating field "{}" in model "{}": {}'.format(
            self.field, self.eclass, self.message
        )
