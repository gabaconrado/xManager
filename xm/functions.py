'''
    file: functions.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the functions for the interface
'''

import getpass
import hashlib

from xm import database as db
from xm.model import User, Account, Dream, Category, Expense, Operation
from xm.util import get_account_data, get_dream_data, get_category_data, \
                    get_expense_data, get_operation_data

# All commands available in the software
commands_strings = ['add', 'edit', 'delete', 'list', 'help', 'purge']

commands_dict = {}


def login():
    '''
    Get the system user from DB, if it does not exists create one
    @params: none
    @return: if login was successful and the user that logged in
    '''
    db.create_database()
    # Create an user if there is none in the DB
    if not User.get():
        print("No user found, creating one...")
        name = input("Type an username: ")
        password = getpass.getpass(
            "Enter password for {}: ".format(name))
        # Hash the password
        password = hashlib.sha256(bytes(password, "utf-8"))
        hashed_pass = password.hexdigest()
        user = User(name=name, password=hashed_pass)
        user.save()
    else:
        name = input("Username: ")
        password = getpass.getpass(
            "Enter password for {}: ".format(name))
        password = hashlib.sha256(bytes(password, "utf-8"))
        hashed_pass = password.hexdigest()
        user = User.get(name=name, password=hashed_pass)
        if user:
            user = user[0]
    return (user is not None), user


def list_commands():
    '''
    Show a list of all available commands
    '''
    print("Available commands:")
    for command in commands_strings:
        print(command)


def purge_app():
    '''
    Cleans all data from database
    '''
    print("Do you really want to erase permanently all app data?")
    if input("Y/N: ").lower() in ['yes', 'y']:
        db.purge_database()
        db.create_database()
        print("Database purged!")
        quit()
    else:
        print("Canceled.")


def prompt_confirm():
    '''
    Prompt user for confirmation
    @params: None
    @return: True if user confirmed, False if not
    '''
    print("Commit to changes?")
    return input('Y/N: ').lower() in ['yes', 'y']


def add_account():
    '''
    Add an account
    @params: None
    @return: None
    '''
    print("Creating new account, please enter required data.")
    account = get_account_data()
    if prompt_confirm():
        account.save()
        print("Account created")
    else:
        print("Account not created")


def edit_account():
    '''
    Edit an account
    @param: none
    @return: None
    '''
    account_id = input("Account ID: ")
    account = Account.get(id=account_id)
    if account:
        account = account[0]
        print("Account {} selected. Enter new data.".format(str(account)))
        new_account = get_account_data()
        if prompt_confirm():
            new_account.id = account.id
            new_account.save()
            print("Account updated")
        else:
            print("Update account canceled")
    else:
        print("Account not found.")


def delete_account():
    '''
    Delete an account
    @params: none
    @return: none
    '''
    account_id = input("Account ID: ")
    accounts = Account.get(id=account_id)
    if accounts:
        account = accounts[0]
        if prompt_confirm():
            account.delete()
            print("Account deleted")
        else:
            print("Account not deleted")
    else:
        print("Account not found")


def list_accounts():
    '''
    List all accounts
    @params: none
    @return: none
    '''
    accounts = Account.get()
    type_account = ['Nada', 'Current', 'Card']
    if accounts:
        print("ID | Alias | Description | Type | Balance")
        for account in accounts:
            print("{} | {} | {} | {} | {}".format(
                str(account.id),
                str(account),
                account.description,
                type_account[account.type],
                str(account.balance)))
    else:
        print("There are not any accounts")


def add_dream():
    '''
    Add a dream
    @params: None
    @return: None
    '''
    dream = get_dream_data()
    if prompt_confirm():
        dream.save()
        print("Dream saved")
    else:
        print("Dream not saved")


def edit_dream():
    '''
    Edit a dream by its ID
    @params: None
    @return: None
    '''
    dream_id = input("Dream ID: ")
    dream = Dream.get(id=dream_id)
    if dream:
        dream = dream[0]
        new_dream = get_dream_data()
        if prompt_confirm():
            new_dream.id = dream.id
            new_dream.save()
        else:
            print("Update dream canceled")
    else:
        print("Dream not found")


def delete_dream():
    '''
    Delete a dream by its ID
    @params: None
    @return: None
    '''
    dream_id = input("Dream ID: ")
    dream = Dream.get(id=dream_id)
    if dream:
        dream = dream[0]
        if prompt_confirm():
            dream.delete()
            print("Dream deleted")
        else:
            print("Dream not deleted")
    else:
        print("Dream not found")


def list_dreams():
    '''
    List dreams
    @params: None
    @return: None
    '''
    dreams = Dream.get()
    if dreams:
        print("ID | Alias | Achieved | Goal | Date")
        for dream in dreams:
            print("{} | {} | {} | {} | {} ".format(
                str(dream.id),
                dream.alias,
                str(dream.achieved),
                str(dream.goal),
                dream.date.strftime("%d/%M/%Y")
            ))
    else:
        print("There are not any dreams")


def add_category():
    '''
    Add a category
    @params: None
    @return: None
    '''
    category = get_category_data()
    if prompt_confirm():
        category.save()
        print("Category added")
    else:
        print("Category not saved")


def edit_category():
    '''
    Edit a category by its ID
    @params: None
    @return: None
    '''
    category_id = input("Category ID: ")
    category = Category.get(id=category_id)
    if category:
        category = category[0]
        new_category = get_category_data()
        if prompt_confirm():
            new_category.id = category.id
            new_category.save()
            print("Category updated")
        else:
            print("Category not updated")
    else:
        print("Category not found")


def delete_category():
    '''
    Delete a category by its ID
    @params: None
    @return: None
    '''
    category_id = input("Category ID: ")
    category = Category.get(id=category_id)
    if category:
        category = category[0]
        if prompt_confirm():
            category.delete()
            print("Category deleted")
        else:
            print("Category not deleted")
    else:
        print("Category not found")


def list_category():
    '''
    List all categories
    @param: None
    @return: None
    '''
    categories = Category.get()
    if categories:
        print("ID | Alias ")
        for category in categories:
            print("{} | {}".format(str(category.id), category.alias))
    else:
        print("There is not any category")


def add_expense():
    '''
    Add an expense
    @param: None
    @return: None
    '''
    expense = get_expense_data()
    if prompt_confirm():
        expense.save()
        print("Expense registered")
    else:
        print("Expense not registered")


def edit_expense():
    '''
    Edit an expense by its ID
    @param: None
    @return: None
    '''
    expense_id = input("Expense ID: ")
    expense = Expense.get(id=expense_id)
    if expense:
        expense = expense[0]
        new_expense = get_expense_data()
        if prompt_confirm():
            new_expense.id = expense.id
            new_expense.save()
            print("Expense updated")
        else:
            print("Expense not updated")
    else:
        print("Expense not found")


def delete_expense():
    '''
    Delete an expense by its ID
    @param: None
    @return: None
    '''
    expense_id = input("Expense ID: ")
    expense = Expense.get(id=expense_id)
    if expense:
        expense = expense[0]
        if prompt_confirm():
            expense.delete()
            print("Expense deleted")
        else:
            print("Expense not deleted")
    else:
        print("Expense not found")


def list_expenses():
    '''
    List all expenses
    @param: None
    @return: None
    '''
    expenses = Expense.get()
    if expenses:
        print("ID | Alias | Value | Date | Account | Category")
        for expense in expenses:
            print("{} | {} | {} | {} | {} | {}".format(
                str(expense.id),
                expense.alias,
                str(expense.value),
                expense.date.strftime("%d/%M/%Y"),
                str(expense.account),
                str(expense.category),
            ))
    else:
        print("There are not any expenses")


def add_operation():
    '''
    Add an operation
    @param: None
    @return: None
    '''
    operation = get_operation_data()
    if prompt_confirm():
        operation.save()
        print("Operation saved")
    else:
        print("Operation not saved")


def edit_operation():
    '''
    Edit an operation by its ID
    @param: None
    @return: None
    '''
    operation_id = input("Operation ID: ")
    operation = Operation.get(id=operation_id)
    if operation:
        operation = operation[0]
        new_operation = get_operation_data()
        if prompt_confirm():
            new_operation.id = operation.id
            new_operation.save()
            print("Operation updated")
        else:
            print("Operation not updated")
    else:
        print("Operation not found")


def delete_operation():
    '''
    Delete an operation by its ID
    @param: None
    @return: None
    '''
    operation_id = input("Operation ID: ")
    operation = Operation.get(id=operation_id)
    if operation:
        operation = operation[0]
        if prompt_confirm():
            operation.delete()
            print("Operation deleted")
        else:
            print("Operation not deleted")
    else:
        print("Operation not found")


def list_operations():
    '''
    List all operations
    @param: None
    @return: None
    '''
    operations = Operation.get()
    if operations:
        print("ID | Source | Destiny | Amount | Date | Description")
        for operation in operations:
            print("{} | {} | {} | {} | {} | {}".format(
                str(operation.id),
                str(operation.source),
                str(operation.destiny),
                str(operation.amount),
                operation.date.strftime("%d/%M/%Y"),
                operation.description
            ))
    else:
        print("There are not any operations")


def build_command_dictionary():
    '''
    Build the commands dictionary
    @params: None
    @return: commands dictionary
    '''
    global commands_dict
    commands_dict = {
        'account': {
            'add': add_account,
            'edit': edit_account,
            'delete': delete_account,
            'list': list_accounts,
        },
        'dream': {
            'add': add_dream,
            'edit': edit_dream,
            'delete': delete_dream,
            'list': list_dreams
        },
        'category': {
            'add': add_category,
            'edit': edit_category,
            'delete': delete_category,
            'list': list_category
        },
        'expense': {
            'add': add_expense,
            'edit': edit_expense,
            'delete': delete_expense,
            'list': list_expenses
        },
        'operation': {
            'add': add_operation,
            'edit': edit_operation,
            'delete': delete_operation,
            'list': list_operations
        },
    }


def parse_inputs(user_input):
    '''
    Parse the user commands and call the currespondent function
    @params: the user command
    @return: none
    '''
    user_input = user_input.split(" ")
    command = user_input[0]
    option = None
    if len(user_input) > 1:
        option = user_input[1]
    if command in commands_strings:
        if command == 'help':
            list_commands()
        elif command == 'purge':
            purge_app()
        elif option and option in commands_dict and \
                command in commands_dict[option]:
            commands_dict[option][command]()
        else:
            print("Malformed command(Invalid options)")
    else:
        print("Invalid command, to see command list type 'help'")
