'''
    file: database.py
    author: gabriel conrado
    project: xManager
    brief:
        Database interface for the system
        The database used in this project will be SQLite3
'''
# pylint: disable = too-many-lines

import os

import sqlite3

from xm.exceptions import DatabaseException, ValidationException


create_table_user_string = '''create table if not exists user(
id integer not null primary key autoincrement,
name text not_null,
password text);'''

create_table_operation_string = '''create table if not exists operation(
id integer not null primary key autoincrement,
source integer not null,
destiny not null,
amount real not null,
date timestamp not null,
description text,
foreign key(source) references account(id) on delete cascade,
foreign key(destiny) references account(id) on delete cascade);'''

create_table_account_string = '''create table if not exists account(
id integer not null primary key autoincrement,
alias text not null,
owner integer not null,
type integer not null,
description text,
balance real,
max_limit real,
due_date integer,
foreign key(owner) references user(id) on delete cascade);'''

create_table_expense_string = '''create table if not exists expense(
id integer not null primary key autoincrement,
alias text not null,
value real not null,
date timestamp not null,
account integer,
category integer,
foreign key(account) references account(id) on delete cascade,
foreign key(category) references category(id) on delete cascade);'''

create_table_category_string = '''create table if not exists category(
id integer not null primary key autoincrement,
alias text not null);'''

create_table_dream_string = '''create table if not exists dream(
id integer not null primary key autoincrement,
alias text not null,
achieved real not null,
goal real not null,
date timestamp not null
);'''

create_database_string = [create_table_user_string,
                          create_table_account_string,
                          create_table_operation_string,
                          create_table_category_string,
                          create_table_expense_string,
                          create_table_dream_string]

drop_table_user_string = 'drop table user;'

drop_table_account_string = 'drop table account;'

drop_table_operation_string = 'drop table operation;'

drop_table_category_string = 'drop table category;'

drop_table_expense_string = 'drop table expense;'

drop_table_dream_string = 'drop table dream;'


purge_database_string = [drop_table_user_string,
                         drop_table_account_string,
                         drop_table_operation_string,
                         drop_table_expense_string,
                         drop_table_category_string,
                         drop_table_dream_string]

user_fields = ("id", "name", "password")

account_fields = {"id", "alias", "owner",
                  "type", "description", "balance",
                  "max_limit", "due_date"}

operation_fields = {"id", "source", "destiny",
                    "amount", "date", "description"}

category_fields = ("id", "alias")

expense_fields = ("id", "alias", "value", "date", "account", "category")

dream_fields = ("id", "alias", "achieved", "goal", "date")

TYPES_ACCOUNT = {"current": 1, "card": 2}

database_file = 'xmanager.db'


def _connect():
    '''
    Connect to the database
    @return: A valid sqlite3 connection
    '''
    conn = sqlite3.connect(database_file, detect_types=sqlite3.PARSE_DECLTYPES)
    return conn


def _disconnect(conn):
    '''
    Disconnect from the database
    @params: The sqlite3 connection
    '''
    conn.close()


def clean_database_file():
    '''
    Erase the database file
    @params: none
    '''
    os.remove(database_file)


def create_database():
    '''
    Runs the script to generate the database
    @params: none
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        cursor = conn.cursor()
        for query in create_database_string:
            cursor.execute(query)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='create database',
            table='xManager database',
            message=str(error)
            )


def purge_database():
    '''
    Runs the scrtipt to generate the database
    @params: none
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        cursor = conn.cursor()
        for query in purge_database_string:
            cursor.execute(query)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='purge database',
            table='xManager database',
            message=str(error)
            )


def insert_user(user_data):
    '''
    Public function to insert an user into database
    @params: dict user_data: the user data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        user_data['id'] = _insert_user(conn=conn, user_data=user_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='insert user',
            table='user',
            message=str(error)
            )


def update_user(user_data):
    '''
    Public function to update an user in
    @params: dict user_data: user parameters
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _update_user(conn=conn, user_data=user_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='update user',
            table='user',
            message=str(error)
            )


def get_user(**kwargs):
    '''
    Public function to retrieve an user from database
    @params: the user parameters
    @return: user dictionary
    @raises: DatabaseException if SQL Operational error
    @raises: ValidationException if wrong arguments
    '''
    for field in kwargs:
        if field not in user_fields:
            raise ValidationException(
                message="Non-existent field",
                eclass="User",
                field=field
                )
    try:
        conn = _connect()
        user = _get_user(conn, **kwargs)
        _disconnect(conn)
        return user
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='get user',
            table='user',
            message=str(error)
            )


def delete_user(user_id):
    '''
    Delete a specific user from the DB
    @params: User id
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _delete_user(conn=conn, user_id=user_id)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='delete user',
            table='user',
            message=str(error)
            )


def insert_account(account_data):
    '''
    Public function to insert an account into database
    @params: dict account_data: the account data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        account_data['id'] = _insert_account(conn=conn,
                                             account_data=account_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='insert account',
            table='account',
            message=str(error)
            )


def update_account(account_data):
    '''
    Public function to update an account in database
    @params: dict account_data: the account data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _update_account(conn=conn, account_data=account_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='update account',
            table='account',
            message=str(error)
            )


def get_account(**kwargs):
    '''
    Public function to retrieve an account from database
    @params: the account parameters
    @return: account dictionary
    @raises: DatabaseException if SQL Operational error
    @raises: ValidationException if wrong arguments
    '''
    for field in kwargs:
        if field not in account_fields:
            raise ValidationException(
                message="Non-existent field",
                eclass="Account",
                field=field
                )
    try:
        conn = _connect()
        account = _get_account(conn, **kwargs)
        _disconnect(conn)
        return account
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='get account',
            table='account',
            message=str(error)
            )


def delete_account(account_id):
    '''
    Delete a specific account from the DB
    @params: Account id
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _delete_account(conn=conn, account_id=account_id)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='delete account',
            table='account',
            message=str(error)
            )


def insert_operation(operation_data):
    '''
    Public function to insert an operation into database
    @params: dict operation_data: the operation data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        operation_data['id'] = _insert_operation(conn=conn,
                                                 operation_data=operation_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='insert operation',
            table='operation',
            message=str(error)
            )


def update_operation(operation_data):
    '''
    Public function to update an operation in database
    @params: dict operation_data: the operation data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _update_operation(conn=conn, operation_data=operation_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='update operation',
            table='operation',
            message=str(error)
            )


def get_operation(**kwargs):
    '''
    Public function to retrieve an operation from database
    @params: the operations parameters
    @return: operation dictionary
    @raises: DatabaseException if SQL Operational error
    @raises: ValidationException if wrong arguments
    '''
    for field in kwargs:
        if field not in operation_fields:
            raise ValidationException(
                message="Non-existent field",
                eclass="User",
                field=field
                )
    try:
        conn = _connect()
        operation = _get_operation(conn, **kwargs)
        _disconnect(conn)
        return operation
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='get operation',
            table='operation',
            message=str(error)
            )


def delete_operation(operation_id):
    '''
    Delete a specific operation from the DB
    @params: Operation id
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _delete_operation(conn=conn, operation_id=operation_id)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='delete operation',
            table='operation',
            message=str(error)
            )


def insert_category(category_data):
    '''
    Public function to insert a category into database
    @params: dict category_data: the category data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        category_data['id'] = _insert_category(
            conn=conn, category_data=category_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='insert category',
            table='category',
            message=str(error)
            )


def update_category(category_data):
    '''
    Public function to update a category in
    @params: dict category_data: category parameters
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _update_category(conn=conn, category_data=category_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='update category',
            table='category',
            message=str(error)
            )


def get_category(**kwargs):
    '''
    Public function to retrieve a category from database
    @params: the category parameters
    @return: category dictionary
    @raises: DatabaseException if SQL Operational error
    @raises: ValidationException if wrong arguments
    '''
    for field in kwargs:
        if field not in category_fields:
            raise ValidationException(
                message="Non-existent field",
                eclass="Category",
                field=field
                )
    try:
        conn = _connect()
        category = _get_category(conn, **kwargs)
        _disconnect(conn)
        return category
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='get category',
            table='category',
            message=str(error)
            )


def delete_category(category_id):
    '''
    Delete a specific category from the DB
    @params: Category id
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _delete_category(conn=conn, category_id=category_id)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='delete category',
            table='category',
            message=str(error)
            )


def insert_expense(expense_data):
    '''
    Public function to insert an expense into database
    @params: dict expense_data: the expense data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        expense_data['id'] = _insert_expense(conn=conn,
                                             expense_data=expense_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='insert expense',
            table='expense',
            message=str(error)
            )


def update_expense(expense_data):
    '''
    Public function to update an expense in database
    @params: dict expense_data: the expense data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _update_expense(conn=conn, expense_data=expense_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='update expense',
            table='expense',
            message=str(error)
            )


def get_expense(**kwargs):
    '''
    Public function to retrieve an expense from database
    @params: the expense parameters
    @return: expense dictionary
    @raises: DatabaseException if SQL Operational error
    @raises: ValidationException if wrong arguments
    '''
    for field in kwargs:
        if field not in expense_fields:
            raise ValidationException(
                message="Non-existent field",
                eclass="Expense",
                field=field
                )
    try:
        conn = _connect()
        expense = _get_expense(conn, **kwargs)
        _disconnect(conn)
        return expense
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='get expense',
            table='expense',
            message=str(error)
            )


def delete_expense(expense_id):
    '''
    Delete a specific expense from the DB
    @params: Expense id
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _delete_expense(conn=conn, expense_id=expense_id)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='delete expense',
            table='expense',
            message=str(error)
            )


def insert_dream(dream_data):
    '''
    Public function to insert a dream into database
    @params: dict dream_data: the dream data
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        dream_data['id'] = _insert_dream(conn=conn, dream_data=dream_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='insert dream',
            table='dream',
            message=str(error)
            )


def update_dream(dream_data):
    '''
    Public function to update a dream in
    @params: dict dream_data: dream parameters
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _update_dream(conn=conn, dream_data=dream_data)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='update dream',
            table='dream',
            message=str(error)
            )


def get_dream(**kwargs):
    '''
    Public function to retrieve a dream from database
    @params: the dream parameters
    @return: dream dictionary
    @raises: DatabaseException if SQL Operational error
    @raises: ValidationException if wrong arguments
    '''
    for field in kwargs:
        if field not in dream_fields:
            raise ValidationException(
                message="Non-existent field",
                eclass="Dream",
                field=field
                )
    try:
        conn = _connect()
        dream = _get_dream(conn, **kwargs)
        _disconnect(conn)
        return dream
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='get dream',
            table='dream',
            message=str(error)
            )


def delete_dream(dream_id):
    '''
    Delete a specific dream from the DB
    @params: Dream id
    @raises: DatabaseException if SQL Operational error
    '''
    try:
        conn = _connect()
        _delete_dream(conn=conn, dream_id=dream_id)
        _disconnect(conn)
    except sqlite3.Error as error:
        raise DatabaseException(
            etype='Query error',
            operation='delete dream',
            table='dream',
            message=str(error)
            )


def _insert_user(conn, user_data):
    '''
    Insert an user to the DB
    @params: dict user_data : data to be inserted and the connection
    @return: none
    '''
    cursor = conn.cursor()
    query = 'insert into user (name, password) values (?, ?)'
    cursor.execute(query, (user_data['name'], user_data['password'],))
    conn.commit()
    return cursor.lastrowid


def _update_user(conn, user_data):
    '''
    Update an user in the DB
    @params: user parameters
    @return: None
    '''
    cursor = conn.cursor()
    query = 'update user set name = ?, password = ? where id = ?'
    cursor.execute(query, (user_data['name'], user_data['password'],
                           user_data['id'],))
    conn.commit()


def _get_user(conn, **kwargs):
    '''
    Get specific users from the DB
    @params: user parameters
    @return: list dict user data: user data or none if no user was found
    '''
    cursor = conn.cursor()
    query = 'select * from user where'
    values = []
    for field in user_fields:
        if field in kwargs:
            query += ' {} = ? and '.format(field)
            values.append(kwargs[field])
    cursor.execute(query[0:-5], tuple(values))
    results = cursor.fetchall()
    users = []
    if results:
        for u in results:
            user_data = {'id': u[0], 'name': u[1], 'password': u[2]}
            users.append(user_data)
    else:
        return None
    return users


def _delete_user(conn, user_id=None):
    '''
    Delete an specific user from the DB
    @params: user id
    @return: None
    '''
    cursor = conn.cursor()
    cursor.execute("PRAGMA foreign_keys = ON")
    query = 'delete from user where id = ?'
    cursor.execute(query, (user_id,))
    conn.commit()


def _insert_account(conn, account_data):
    '''
    Insert an account to the DB
    @params: dict account_data : data to be inserted and the connection
    @return: none
    '''
    cursor = conn.cursor()
    fields_str = 'alias, owner, type, description, balance'
    values = [account_data['alias'],
              account_data['owner']['id'],
              account_data['type'],
              account_data['description'],
              account_data['balance']]
    if account_data['type'] == TYPES_ACCOUNT['card']:
        fields_str += ', max_limit, due_date'
        values.append(account_data['max_limit'])
        values.append(account_data['due_date'])
    n_values = len(values) - 1
    query = "insert into account ({}) values (?{})".format(fields_str,
                                                           (n_values * ', ?'))
    cursor.execute(query, tuple(values))
    conn.commit()
    return cursor.lastrowid


def _update_account(conn, account_data):
    '''
    update an account in the DB
    @params: dict account_data : data to be updated and the connection
    @return: none
    '''
    cursor = conn.cursor()
    fields_str = 'alias = ?, owner = ?, type = ?, description = ?, balance = ?'
    values = [account_data['alias'],
              account_data['owner']['id'],
              account_data['type'],
              account_data['description'],
              account_data['balance']]
    if account_data['type'] == TYPES_ACCOUNT['card']:
        fields_str += ', max_limit = ?, due_date = ?'
        values.append(account_data['max_limit'])
        values.append(account_data['due_date'])
    values.append(account_data['id'])
    query = 'update account set {} where id = ?'.format(fields_str)
    cursor.execute(query, tuple(values))
    conn.commit()


# pylint: disable=too-many-locals,too-many-branches
def _get_account(conn, **kwargs):
    '''
    Get specific accounts from the DB
    @params: account parameters
    @return: list dict account data: account data
             or none if no account was found
    '''
    cursor = conn.cursor()
    query = "select * from account where"
    values = []
    for field in account_fields:
        if field in kwargs:
            query += ' {} = ? and '.format(field)
            values.append(kwargs[field])
    cursor.execute(query[0:-5], tuple(values))
    results = cursor.fetchall()
    if results:
        accounts = []
        for a in results:
            user = _get_user(conn=conn, id=a[2])[0]
            type_account = a[3]
            account = {'id': a[0], 'alias': a[1], 'owner': user,
                       'description': a[4], 'balance': a[5],
                       'type': type_account}
            if type_account == TYPES_ACCOUNT['card']:
                account['max_limit'] = a[6]
                account['due_date'] = a[7]
            accounts.append(account)
    else:
        return None
    return accounts


def _delete_account(conn, account_id):
    '''
    Delete an specific account from the DB
    @params: Account id
    @return: none
    '''
    cursor = conn.cursor()
    cursor.execute("PRAGMA foreign_keys = ON")
    query = 'delete from account where id = ?'
    cursor.execute(query, (account_id,))
    conn.commit()


def _insert_operation(conn, operation_data):
    '''
    Insert an operation to the DB
    @params: dict operation_data : data to be inserted and the connection
    @return: none
    '''
    cursor = conn.cursor()
    query = 'insert into operation (source, destiny, amount, date, \
            description) values (?, ?, ?, ?, ?)'
    cursor.execute(query, (operation_data['source']['id'],
                           operation_data['destiny']['id'],
                           operation_data['amount'], operation_data['date'],
                           operation_data['description']))
    conn.commit()
    return cursor.lastrowid


def _update_operation(conn, operation_data):
    '''
    Update an operation in the DB
    @params: dict operation_data : data to be updated and the connection
    @return: none
    '''
    cursor = conn.cursor()
    query = 'update operation set source = ?, destiny = ?, amount = ?, \
             date = ?, description = ? where id = ?'
    cursor.execute(query, (operation_data['source']['id'],
                           operation_data['destiny']['id'],
                           operation_data['amount'],
                           operation_data['date'],
                           operation_data['description'],
                           operation_data['id']))
    conn.commit()
    return cursor.lastrowid


def _get_operation(conn, **kwargs):
    '''
    Get specific operations from the DB
    @params: none
    @return: list dict operation data: operation data
             or none if no user was found
    '''
    cursor = conn.cursor()
    query = 'select * from operation where'
    values = []
    for field in operation_fields:
        if field in kwargs:
            query += ' {} = ? and '.format(field)
            values.append(kwargs[field])
    cursor.execute(query[0:-5], tuple(values))
    results = cursor.fetchall()
    if results:
        operations = []
        for o in results:
            source = _get_account(conn, id=o[1])[0]
            destiny = _get_account(conn, id=o[2])[0]
            operation = {'id': o[0], 'source': source, 'destiny': destiny,
                         'amount': o[3], 'date': o[4], 'description': o[5]}
            operations.append(operation)
    else:
        return None
    return operations


def _delete_operation(conn, operation_id):
    '''
    Delete an specific operation from the DB
    @params: Operation id
    @return: none
    '''
    cursor = conn.cursor()
    query = 'delete from operation where id = ?'
    cursor.execute(query, (operation_id,))
    conn.commit()


def _insert_category(conn, category_data):
    '''
    Insert an category to the DB
    @params: dict category_data : data to be inserted and the connection
    @return: none
    '''
    cursor = conn.cursor()
    query = 'insert into category (alias) values (?)'
    cursor.execute(query, (category_data['alias'],))
    conn.commit()
    return cursor.lastrowid


def _update_category(conn, category_data):
    '''
    Update an category in the DB
    @params: category parameters
    @return: None
    '''
    cursor = conn.cursor()
    query = 'update category set alias = ? where id = ?'
    cursor.execute(query, (category_data['alias'], category_data['id'],))
    conn.commit()


def _get_category(conn, **kwargs):
    '''
    Get specific categorys from the DB
    @params: category parameters
    @return: list dict category data: category data or
                                      none if no category was found
    '''
    cursor = conn.cursor()
    query = 'select * from category where'
    values = []
    for field in category_fields:
        if field in kwargs:
            query += ' {} = ? and '.format(field)
            values.append(kwargs[field])
    cursor.execute(query[0:-5], tuple(values))
    results = cursor.fetchall()
    categories = []
    if results:
        for c in results:
            category_data = {'id': c[0], 'alias': c[1]}
            categories.append(category_data)
    else:
        return None
    return categories


def _delete_category(conn, category_id):
    '''
    Delete an specific category from the DB
    @params: Category id
    @return: none
    '''
    cursor = conn.cursor()
    cursor.execute("PRAGMA foreign_keys = ON")
    query = 'delete from category where id = ?'
    cursor.execute(query, (category_id,))
    conn.commit()


def _insert_expense(conn, expense_data):
    '''
    Insert an expense to the DB
    @params: dict expense_data : data to be inserted and the connection
    @return: none
    '''
    cursor = conn.cursor()
    values = [expense_data['alias'],
              expense_data['value'],
              expense_data['date'],
              expense_data['account']['id'],
              expense_data['category']['id']]
    query = "insert into expense (alias, value, date, account, category) \
             values (?, ?, ?, ?, ?)"
    cursor.execute(query, tuple(values))
    conn.commit()
    return cursor.lastrowid


def _update_expense(conn, expense_data):
    '''
    update an expense in the DB
    @params: dict expense_data : data to be updated and the connection
    @return: none
    '''
    cursor = conn.cursor()
    values = [expense_data['alias'],
              expense_data['value'],
              expense_data['date'],
              expense_data['account']['id'],
              expense_data['category']['id'],
              expense_data['id']]
    query = 'update expense set alias = ?, value = ?, date = ?, account = ?,\
             category = ? where id = ?'
    cursor.execute(query, tuple(values))
    conn.commit()


# pylint: disable=too-many-locals,too-many-branches
def _get_expense(conn, **kwargs):
    '''
    Get specific expenses from the DB
    @params: expense parameters
    @return: list dict expense data: expense data
             or none if no expense was found
    '''
    cursor = conn.cursor()
    query = "select * from expense where"
    values = []
    for field in expense_fields:
        if field in kwargs:
            query += ' {} = ? and '.format(field)
            values.append(kwargs[field])
    cursor.execute(query[0:-5], tuple(values))
    results = cursor.fetchall()
    if results:
        expenses = []
        for e in results:
            category = _get_category(conn=conn, id=e[5])[0]
            account = _get_account(conn=conn, id=e[4])[0]
            expense = {'id': e[0], 'alias': e[1],
                       'value': e[2], 'date': e[3],
                       'account': account, 'category': category}
            expenses.append(expense)
    else:
        return None
    return expenses


def _delete_expense(conn, expense_id):
    '''
    Delete an specific expense from the DB
    @params: Expense id
    @return: none
    '''
    cursor = conn.cursor()
    query = 'delete from expense where id = ?'
    cursor.execute(query, (expense_id,))
    conn.commit()


def _insert_dream(conn, dream_data):
    '''
    Insert a dream to the DB
    @params: dict dream_data : data to be inserted and the connection
    @return: none
    '''
    cursor = conn.cursor()
    query = 'insert into dream (alias, achieved, goal, date) \
             values (?, ?, ?, ?)'
    cursor.execute(query, (dream_data['alias'],
                           dream_data['achieved'],
                           dream_data['goal'],
                           dream_data['date'],))
    conn.commit()
    return cursor.lastrowid


def _update_dream(conn, dream_data):
    '''
    Update an dream in the DB
    @params: dream parameters
    @return: None
    '''
    cursor = conn.cursor()
    query = 'update dream set alias = ?, achieved = ? , goal = ?, date = ? \
             where id = ?'
    cursor.execute(query, (dream_data['alias'], dream_data['achieved'],
                           dream_data['goal'], dream_data['date'],
                           dream_data['id'],))
    conn.commit()


def _get_dream(conn, **kwargs):
    '''
    Get specific dreams from the DB
    @params: dream parameters
    @return: list dict dream data: dream data or none if no dream was found
    '''
    cursor = conn.cursor()
    query = 'select * from dream where'
    values = []
    for field in dream_fields:
        if field in kwargs:
            query += ' {} = ? and '.format(field)
            values.append(kwargs[field])
    cursor.execute(query[0:-5], tuple(values))
    results = cursor.fetchall()
    dreams = []
    if results:
        for d in results:
            dream_data = {'id': d[0], 'alias': d[1], 'achieved': d[2],
                          'goal': d[3], 'date': d[4]}
            dreams.append(dream_data)
    else:
        return None
    return dreams


def _delete_dream(conn, dream_id=None):
    '''
    Delete a specific dream from the DB
    @params: dream id
    @return: None
    '''
    cursor = conn.cursor()
    query = 'delete from dream where id = ?'
    cursor.execute(query, (dream_id,))
    conn.commit()
