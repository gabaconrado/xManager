'''
    file: util.py
    author: gabriel conrado
    project: xManager
    brief:
        Util strings and settings file
'''

from datetime import datetime
from xm.model import User, Account, Current, Card, Dream, Category, Expense,\
                     Operation


def get_main_user():
    '''
    Function to quickly retrieve main user
    @param: None
    @return: the main user
    '''
    main_user = User.get()
    if main_user:
        return main_user[0]
    return None


def get_account_data():
    '''
    Function to prompt account data
    @param: none
    @return: account object
    '''
    alias = input("Alias: ")
    description = input("Description: ")
    owner = get_main_user()
    while True:
        type_account = input("Type(Current or Card): ")
        if type_account.lower() not in ['current', 'card']:
            print("Invalid type! Try again.")
            continue
        elif type_account.lower() == "current":
            account = Current(
                alias=alias,
                owner=owner,
                description=description,
                balance=0.0
                )
        elif type_account.lower() == "card":
            limit = float(input("Card limit: "))
            due_date = int(input("Invoice due month day: "))
            account = Card(
                alias=alias,
                owner=owner,
                description=description,
                max_limit=limit,
                due_date=due_date,
                balance=limit
                )
        break
    return account


def get_dream_data():
    '''
    Function to prompt for dream data
    @param: None
    @return: the dream object
    '''
    while True:
        try:
            alias = input("Name: ")
            goal = float(input("Goal(R$): "))
            date = datetime.strptime(input("Date(dd/mm/yyyy): "), "%d/%M/%Y")
            dream = Dream(alias, 0.0, goal, date)
            break
        except ValueError:
            print("Invalid value, try again")
            continue
    return dream


def get_category_data():
    '''
    Function do prompt for category data
    @param: None
    @return: the category object
    '''
    alias = input("Name: ")
    category = Category(alias=alias)
    return category


def get_expense_data():
    '''
    Function to prompt for expense data
    @param: None
    @return: None
    '''
    while True:
        try:
            alias = input("Name: ")
            value = float(input("Value(R$): "))
            date = datetime.strptime(input("Date(dd/mm/yyyy): "), "%d/%M/%Y")
            account_id = int(input("Account ID: "))
            category_id = int(input("Category ID: "))
            account = Account.get(id=account_id)
            category = Category.get(id=category_id)
            if not account or not category:
                raise ValueError
            expense = Expense(alias, value, date, account[0], category[0])
            break
        except ValueError:
            print("Invalid value, try again")
            continue
    return expense


def get_operation_data():
    '''
    Function to prompt for operation data
    @param: None
    @return: None
    '''
    while True:
        try:
            source = int(input("Source account ID: "))
            destiny = int(input("Destiny account ID: "))
            amount = float(input("Amount(R$): "))
            date = datetime.strptime(input("Date(dd/mm/yyyy): "), "%d/%M/%Y")
            description = input("Description: ")
            source = Account.get(id=source)
            destiny = Account.get(id=destiny)
            if not source or not destiny:
                raise ValueError
            operation = Operation(
                source[0], destiny[0], amount, date, description
            )
            break
        except ValueError:
            print("Invalud value, try again")
            continue
    return operation
