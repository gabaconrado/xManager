'''
    file: utils.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the utility functions for testing
'''

import datetime

from xm.model import User, Account, Current, Card, Operation, Category, \
                     Dream, Expense
from xm.database import TYPES_ACCOUNT


# User utils
def clean_users():
    '''
    Clean all users from db
    '''
    users = User.get()
    if users:
        for user in users:
            user.delete()


def create_user(name='Neymar Jr', password="1234"):
    '''
    Create a generic user
    '''
    user = User(name=name, password=password)
    user.save()
    return user


# Account utils
def clean_accounts():
    '''
    Clean all accounts from db
    '''
    accounts = Account.get()
    if accounts:
        for account in accounts:
            account.delete()


def create_account(owner, alias="Alias", type_account=TYPES_ACCOUNT['current'],
                   description='description', balance=0.0,
                   max_limit=800.0, due_date=10):
    '''
    Create a generic account
    '''
    if type_account == TYPES_ACCOUNT['current']:
        account = Current(
            alias=alias,
            owner=owner,
            description=description,
            balance=balance
            )
    else:
        account = Card(
            alias=alias,
            owner=owner,
            description=description,
            max_limit=max_limit,
            due_date=due_date,
            balance=balance
            )
    account.save()
    return account


# Operation utils
def clean_operations():
    '''
    Clean all operations from db
    '''
    operations = Operation.get()
    if operations:
        for operation in operations:
            operation.delete()


def create_operation(source, destiny, amount=200.0, description="desc"):
    '''
    Create a generic operation
    '''
    date = datetime.datetime.now()
    operation = Operation(
        source=source,
        destiny=destiny,
        amount=amount,
        date=date,
        description=description
        )
    operation.save()
    return operation


# Category utils
def clean_categories():
    '''
    Clean all categories from db
    '''
    categories = Category.get()
    if categories:
        for category in categories:
            category.delete()


def create_category(alias="Steam"):
    '''
    Create a generic category
    '''
    category = Category(alias=alias)
    category.save()
    return category


# Dream utils
def clean_dreams():
    '''
    Clean all dreams from db
    '''
    dreams = Dream.get()
    if dreams:
        for dream in dreams:
            dream.delete()


def create_dream(alias='Kimy', achieved=500.0, goal=1000.0):
    '''
    Create a generic dream
    '''
    date = datetime.datetime.now()
    dream = Dream(
        alias=alias,
        achieved=achieved,
        goal=goal,
        date=date
        )
    dream.save()
    return dream


# Expense utils
def clean_expenses():
    '''
    Clean all expenses from db
    '''
    expenses = Expense.get()
    if expenses:
        for expense in expenses:
            expense.delete()


def create_expense(account, category, alias='Expense', value=100.0):
    '''
    Create a generic expense
    '''
    date = datetime.datetime.now()
    expense = Expense(
        alias=alias,
        account=account,
        category=category,
        date=date,
        value=value
        )
    expense.save()
    return expense
