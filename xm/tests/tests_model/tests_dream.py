'''
    file: tests_dream.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the functions for testing the dream models
'''

import unittest

from xm import database as db
from xm.model import Dream
from xm.exceptions import ValidationException
from xm.tests.utils import create_dream, clean_dreams


class DreamTests(unittest.TestCase):
    '''
    Tests for the Dream models
    '''

    @classmethod
    def setUpClass(cls):
        '''
        Setup class data
        '''
        super(DreamTests, cls).setUpClass()
        db.database_file = 'xmanager_test.db'
        db.create_database()

    @classmethod
    def tearDownClass(cls):
        '''
        Clean the class data
        '''
        db.clean_database_file()
        super(DreamTests, cls).tearDownClass()

    def setUp(self):
        '''
        Setup test data
        '''
        super(DreamTests, self).setUp()
        self.dream = create_dream()

    def tearDown(self):
        '''
        Clean test data
        '''
        clean_dreams()
        super(DreamTests, self).tearDown()

    def test_create_dream(self):
        '''
        Test creating dreams:
            - valid
            - invalid
        '''
        old_id = self.dream.id

        # Invalid ID
        self.dream.id = '1234'
        with self.assertRaises(ValidationException):
            self.dream.save()

        self.dream.id = 0
        with self.assertRaises(ValidationException):
            self.dream.save()

        # Invalid alias
        self.dream.id = old_id
        self.dream.alias = 1234
        with self.assertRaises(ValidationException):
            self.dream.save()

        # Invalid achieved
        self.dream.alias = 'alias'
        self.dream.achieved = '1234'
        with self.assertRaises(ValidationException):
            self.dream.save()

        # Invalid goal
        self.dream.achieved = 1000.0
        self.dream.goal = '1234'
        with self.assertRaises(ValidationException):
            self.dream.save()

        # Invalid date
        self.dream.goal = 1000.0
        self.dream.date = '1234'
        with self.assertRaises(ValidationException):
            self.dream.save()

    def test_getting_dream(self):
        '''
        Test getting an dream from the db
        '''
        dreams = Dream.get()
        self.assertIsNotNone(dreams)
        dream_db = dreams[0]

        self.assertEqual(self.dream.id, dream_db.id)
        self.assertEqual(self.dream.alias, dream_db.alias)
        self.assertEqual(self.dream.achieved, dream_db.achieved)
        self.assertEqual(self.dream.goal, dream_db.goal)
        self.assertEqual(self.dream.date, dream_db.date)

    def test_deleting_dream(self):
        '''
        Test deleting an dream from the db
        '''
        dreams = Dream.get()
        self.assertIsNotNone(dreams)

        self.dream.delete()

        dreams = Dream.get()
        self.assertIsNone(dreams)

    def test_updating_dream(self):
        '''
        Test updating an dream in the db
        '''
        old_id = self.dream.id
        old_alias = self.dream.alias

        self.dream.alias = "João bolão's dream"
        self.dream.save()

        self.assertEqual(old_id, self.dream.id)
        self.assertIsNotNone(Dream.get(alias="João bolão's dream"))
        self.assertIsNone(Dream.get(alias=old_alias))

    def test_dream_to_dictionary(self):
        '''
        Test dream to dictionary function
        '''
        dream_dictionary = self.dream.to_dict()
        self.assertEqual(len(dream_dictionary), 5)
        self.assertIn('id', dream_dictionary)
        self.assertIn('alias', dream_dictionary)
        self.assertIn('achieved', dream_dictionary)
        self.assertIn('goal', dream_dictionary)
        self.assertIn('date', dream_dictionary)
