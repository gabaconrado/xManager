'''
    file: tests_category.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the functions for testing the category models
'''

import unittest

from xm import database as db
from xm.model import Category
from xm.exceptions import ValidationException
from xm.tests.utils import create_category, clean_categories


class CategoryTests(unittest.TestCase):
    '''
    Tests for the Category models
    '''

    @classmethod
    def setUpClass(cls):
        '''
        Setup class data
        '''
        super(CategoryTests, cls).setUpClass()
        db.database_file = 'xmanager_test.db'
        db.create_database()

    @classmethod
    def tearDownClass(cls):
        '''
        Clean the class data
        '''
        db.clean_database_file()
        super(CategoryTests, cls).tearDownClass()

    def setUp(self):
        '''
        Setup test data
        '''
        super(CategoryTests, self).setUp()
        self.category = create_category()

    def tearDown(self):
        '''
        Clean test data
        '''
        clean_categories()
        super(CategoryTests, self).tearDown()

    def test_create_category(self):
        '''
        Test creating categories:
            - valid
            - invalid
        '''
        old_id = self.category.id

        # Invalid ID
        self.category.id = '1234'
        with self.assertRaises(ValidationException):
            self.category.save()

        self.category.id = 0
        with self.assertRaises(ValidationException):
            self.category.save()

        # Invalid alias
        self.category.id = old_id
        self.category.alias = 1234
        with self.assertRaises(ValidationException):
            self.category.save()

    def test_getting_category(self):
        '''
        Test getting an category from the db
        '''
        categorys = Category.get()
        self.assertIsNotNone(categorys)
        category_db = categorys[0]

        self.assertEqual(self.category.id, category_db.id)
        self.assertEqual(self.category.alias, category_db.alias)

    def test_deleting_category(self):
        '''
        Test deleting an category from the db
        '''
        categories = Category.get()
        self.assertIsNotNone(categories)

        self.category.delete()

        categories = Category.get()
        self.assertIsNone(categories)

    def test_updating_category(self):
        '''
        Test updating an category in the db
        '''
        old_id = self.category.id
        old_alias = self.category.alias

        self.category.alias = "João bolão's category"
        self.category.save()

        self.assertEqual(old_id, self.category.id)
        self.assertIsNotNone(Category.get(alias="João bolão's category"))
        self.assertIsNone(Category.get(alias=old_alias))

    def test_category_to_dictionary(self):
        '''
        Test category to dictionary function
        '''
        category_dictionary = self.category.to_dict()
        self.assertEqual(len(category_dictionary), 2)
        self.assertIn('id', category_dictionary)
        self.assertIn('alias', category_dictionary)
