'''
    file: tests_expense.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the functions for testing the expense models
'''

import unittest

from xm import database as db
from xm.model import Expense
from xm.exceptions import ValidationException
from xm.tests.utils import create_account, create_user, create_expense,\
    create_category, clean_users, clean_accounts, clean_categories, \
    clean_expenses


class ExpenseTests(unittest.TestCase):
    '''
    Tests for the Expense models
    '''

    @classmethod
    def setUpClass(cls):
        '''
        Setup class data
        '''
        super(ExpenseTests, cls).setUpClass()
        db.database_file = 'xmanager_test.db'
        db.create_database()
        cls.user = create_user()
        cls.account = create_account(cls.user, alias="Account")
        cls.category = create_category()

    @classmethod
    def tearDownClass(cls):
        '''
        Clean the class data
        '''
        clean_categories()
        clean_accounts()
        clean_users()
        db.clean_database_file()
        super(ExpenseTests, cls).tearDownClass()

    def setUp(self):
        '''
        Setup test data
        '''
        super(ExpenseTests, self).setUp()
        self.expense = create_expense(
            account=self.account,
            category=self.category,
            value=200.0,
            alias="alias"
            )

    def tearDown(self):
        '''
        Clean test data
        '''
        clean_expenses()
        super(ExpenseTests, self).tearDown()

    def test_create_expense(self):
        '''
        Test the creation of expense model
        '''
        old_id = self.expense.id

        # Invalid expenses
        self.expense.id = 0
        with self.assertRaises(ValidationException):
            self.expense.save()

        self.expense.id = '1234'
        with self.assertRaises(ValidationException):
            self.expense.save()

        self.expense.id = old_id
        self.expense.account = 'account'
        with self.assertRaises(ValidationException):
            self.expense.save()

        self.expense.account = self.account
        self.expense.value = '200.0'
        with self.assertRaises(ValidationException):
            self.expense.save()

        self.expense.value = 200.0
        self.expense.alias = 123
        with self.assertRaises(ValidationException):
            self.expense.save()

        self.expense.alias = 'alias'
        self.expense.category = 'category'
        with self.assertRaises(ValidationException):
            self.expense.save()

        self.expense.category = self.category
        self.expense.date = 'date'
        with self.assertRaises(ValidationException):
            self.expense.save()

    def test_getting_expense(self):
        '''
        Test getting an expense from the db
        '''
        expenses = Expense.get()
        self.assertIsNotNone(expenses)
        expense_db = expenses[0]
        self.assertEqual(self.expense.id, expense_db.id)
        self.assertEqual(self.expense.category.id, expense_db.category.id)
        self.assertEqual(self.expense.account.id, expense_db.account.id)
        self.assertEqual(self.expense.value, expense_db.value)
        self.assertEqual(self.expense.alias, expense_db.alias)
        self.assertEqual(self.expense.date, expense_db.date)

    def test_deleting_expense(self):
        '''
        Test deleting an expense from the db
        '''
        expenses = Expense.get()
        self.assertIsNotNone(expenses)

        self.expense.delete()

        expenses = Expense.get()
        self.assertIsNone(expenses)

    def test_updating_expense(self):
        '''
        Test updating an expense in the db
        '''
        old_id = self.expense.id
        old_value = self.expense.value

        self.expense.value = 800.0
        self.expense.save()

        self.assertEqual(old_id, self.expense.id)
        self.assertIsNotNone(Expense.get(value=800.0))
        self.assertIsNone(Expense.get(value=old_value))

    def test_expense_to_dictionary(self):
        '''
        Test expense to dictionary function
        '''
        expense_dictionary = self.expense.to_dict()
        self.assertEqual(len(expense_dictionary), 6)
        self.assertIn('id', expense_dictionary)
        self.assertIn('account', expense_dictionary)
        self.assertIn('category', expense_dictionary)
        self.assertIn('value', expense_dictionary)
        self.assertIn('date', expense_dictionary)
        self.assertIn('alias', expense_dictionary)
