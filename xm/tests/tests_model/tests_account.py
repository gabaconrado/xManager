'''
    file: tests_account.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the functions for testing the account models
'''

import unittest

from xm.database import TYPES_ACCOUNT

from xm import database as db
from xm.model import Account
from xm.exceptions import ValidationException
from xm.tests.utils import clean_accounts, clean_users, \
    create_user, create_account


class CurrentTests(unittest.TestCase):
    '''
    Tests for the Card model
    '''

    @classmethod
    def setUpClass(cls):
        '''
        Setup class data
        '''
        super(CurrentTests, cls).setUpClass()
        db.database_file = 'xmanager_test.db'
        db.create_database()
        cls.user = create_user()

    @classmethod
    def tearDownClass(cls):
        '''
        Clean the class data
        '''
        clean_users()
        db.clean_database_file()
        super(CurrentTests, cls).tearDownClass()

    def setUp(self):
        '''
        Setup current test data
        '''
        super(CurrentTests, self).setUp()
        self.current = create_account(
            owner=self.user,
            alias="Current",
            type_account=TYPES_ACCOUNT['current'],
            description='description',
            balance=1000.0
            )

    def tearDown(self):
        '''
        Clean test data
        '''
        clean_accounts()
        super(CurrentTests, self).tearDown()

    def test_create_current(self):
        '''
        Test creating current accounts:
            - valid
            - invalid
        '''
        old_id = self.current.id

        # Invalid accounts
        self.current.id = 0
        with self.assertRaises(ValidationException):
            self.current.save()

        self.current.id = '0'
        with self.assertRaises(ValidationException):
            self.current.save()

        self.current.id = old_id
        self.current.owner = 'owner'
        with self.assertRaises(ValidationException):
            self.current.save()

        self.current.owner = self.user
        self.current.alias = 123
        with self.assertRaises(ValidationException):
            self.current.save()

        self.current.alias = 'Current'
        self.current.description = 123
        with self.assertRaises(ValidationException):
            self.current.save()

        self.current.description = 'Description'
        self.current.balance = '1000.0'
        with self.assertRaises(ValidationException):
            self.current.save()

    def test_getting_current(self):
        '''
        Test getting a current account from the db
        '''
        accounts = Account.get()
        self.assertIsNotNone(accounts)

        current_db = Account.get(id=self.current.id)[0]

        self.assertEqual(self.current.id, current_db.id)
        self.assertEqual(self.current.alias, current_db.alias)
        self.assertEqual(self.current.owner.id, current_db.owner.id)
        self.assertEqual(self.current.type, current_db.type)
        self.assertEqual(self.current.description, current_db.description)
        self.assertEqual(self.current.balance, current_db.balance)

    def test_deleting_current(self):
        '''
        Test deleting a current account from the db
        '''
        accounts = Account.get()
        self.assertIsNotNone(accounts)

        self.current.delete()

        accounts = Account.get()
        self.assertIsNone(accounts)

    def test_updating_current(self):
        '''
        Test updating a current account in the db
        '''
        old_current_id = self.current.id
        old_current_alias = self.current.alias

        self.current.alias = 'New Current'

        self.current.save()

        self.assertEqual(old_current_id, self.current.id)

        self.assertIsNotNone(Account.get(alias='New Current'))
        self.assertIsNone(Account.get(alias=old_current_alias))

    def test_current_to_dictionary(self):
        '''
        Test current account to dictionary function
        '''
        current_dictionary = self.current.to_dict()
        self.assertEqual(len(current_dictionary), 6)
        self.assertIn('id', current_dictionary)
        self.assertIn('alias', current_dictionary)
        self.assertIn('owner', current_dictionary)
        self.assertIn('description', current_dictionary)
        self.assertIn('balance', current_dictionary)
        self.assertIn('type', current_dictionary)


class CardTests(unittest.TestCase):
    '''
    Tests for the Card model
    '''

    @classmethod
    def setUpClass(cls):
        '''
        Setup class data
        '''
        super(CardTests, cls).setUpClass()
        db.database_file = 'xmanager_test.db'
        db.create_database()
        cls.user = create_user()

    @classmethod
    def tearDownClass(cls):
        '''
        Clean the class data
        '''
        clean_users()
        db.clean_database_file()
        super(CardTests, cls).tearDownClass()

    def setUp(self):
        '''
        Setup card test data
        '''
        super(CardTests, self).setUp()
        self.card = create_account(
            owner=self.user,
            alias="Card",
            type_account=TYPES_ACCOUNT['card'],
            description='description',
            balance=800.0,
            max_limit=800.0,
            due_date=10
            )

    def tearDown(self):
        '''
        Clean test data
        '''
        clean_accounts()
        super(CardTests, self).tearDown()

    def test_create_card(self):
        '''
        Test creating card accounts:
            - valid
            - invalid
        '''
        old_id = self.card.id

        # Invalid accounts
        self.card.id = '1234'
        with self.assertRaises(ValidationException):
            self.card.save()

        self.card.id = 0
        with self.assertRaises(ValidationException):
            self.card.save()

        self.card.id = old_id
        self.card.alias = 123
        with self.assertRaises(ValidationException):
            self.card.save()

        self.card.alias = 'Card'
        self.card.owner = 'owner'
        with self.assertRaises(ValidationException):
            self.card.save()

        self.card.owner = self.user
        self.card.description = 123
        with self.assertRaises(ValidationException):
            self.card.save()

        self.card.description = 'Description'
        self.card.max_limit = '800.0'
        with self.assertRaises(ValidationException):
            self.card.save()

        self.card.max_limit = 800.0
        self.card.due_date = '20'
        with self.assertRaises(ValidationException):
            self.card.save()

        self.card.due_date = 20
        self.card.balance = '800.0'
        with self.assertRaises(ValidationException):
            self.card.save()

    def test_getting_card(self):
        '''
        Test getting a card account from the db
        '''
        accounts = Account.get()
        self.assertIsNotNone(accounts)

        card_db = Account.get(id=self.card.id)[0]

        self.assertEqual(self.card.id, card_db.id)
        self.assertEqual(self.card.alias, card_db.alias)
        self.assertEqual(self.card.owner.id, card_db.owner.id)
        self.assertEqual(self.card.type, card_db.type)
        self.assertEqual(self.card.description, card_db.description)
        self.assertEqual(self.card.max_limit, card_db.max_limit)
        self.assertEqual(self.card.due_date, card_db.due_date)
        self.assertEqual(self.card.balance, card_db.balance)

    def test_deleting_card(self):
        '''
        Test deleting a card account from the db
        '''
        accounts = Account.get()
        self.assertIsNotNone(accounts)

        self.card.delete()

        accounts = Account.get()
        self.assertIsNone(accounts)

    def test_updating_card(self):
        '''
        Test updating a card account in the db
        '''
        old_card_id = self.card.id
        old_card_alias = self.card.alias

        self.card.alias = 'New Card'

        self.card.save()

        self.assertEqual(old_card_id, self.card.id)

        self.assertIsNotNone(Account.get(alias='New Card'))
        self.assertIsNone(Account.get(alias=old_card_alias))

    def test_card_to_dictionary(self):
        '''
        Test card account to dictionary function
        '''
        card_dictionary = self.card.to_dict()
        self.assertEqual(len(card_dictionary), 8)
        self.assertIn('id', card_dictionary)
        self.assertIn('alias', card_dictionary)
        self.assertIn('owner', card_dictionary)
        self.assertIn('description', card_dictionary)
        self.assertIn('balance', card_dictionary)
        self.assertIn('due_date', card_dictionary)
        self.assertIn('max_limit', card_dictionary)
        self.assertIn('type', card_dictionary)
