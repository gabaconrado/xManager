'''
    file: tests_user.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the functions for testing the user models
'''

import unittest

from xm import database as db
from xm.model import User
from xm.exceptions import ValidationException
from xm.tests.utils import create_user, clean_users


class UserTests(unittest.TestCase):
    '''
    Tests for the User models
    '''

    @classmethod
    def setUpClass(cls):
        '''
        Setup class data
        '''
        super(UserTests, cls).setUpClass()
        db.database_file = 'xmanager_test.db'
        db.create_database()

    @classmethod
    def tearDownClass(cls):
        '''
        Clean the class data
        '''
        db.clean_database_file()
        super(UserTests, cls).tearDownClass()

    def setUp(self):
        '''
        Setup test data
        '''
        super(UserTests, self).setUp()
        self.user = create_user()

    def tearDown(self):
        '''
        Clean test data
        '''
        clean_users()
        super(UserTests, self).tearDown()

    def test_create_user(self):
        '''
        Test creating users:
            - valid
            - invalid
        '''
        old_id = self.user.id

        # Invalid ID
        self.user.id = '1234'
        with self.assertRaises(ValidationException):
            self.user.save()

        self.user.id = 0
        with self.assertRaises(ValidationException):
            self.user.save()

        # Invalid password
        self.user.id = old_id
        self.user.password = 1234
        with self.assertRaises(ValidationException):
            self.user.save()

        # Invalid username
        self.user.password = '1234'
        self.user.name = 1234
        with self.assertRaises(ValidationException):
            self.user.save()

    def test_getting_user(self):
        '''
        Test getting an user from the db
        '''
        users = User.get()
        self.assertIsNotNone(users)
        user_db = users[0]

        self.assertEqual(self.user.id, user_db.id)
        self.assertEqual(self.user.name, user_db.name)
        self.assertEqual(self.user.password, user_db.password)

    def test_deleting_user(self):
        '''
        Test deleting an user from the db
        '''
        users = User.get()
        self.assertIsNotNone(users)

        self.user.delete()

        users = User.get()
        self.assertIsNone(users)

    def test_updating_user(self):
        '''
        Test updating an user in the db
        '''
        old_id = self.user.id
        old_name = self.user.name

        self.user.name = 'joao bolao'
        self.user.save()

        self.assertEqual(old_id, self.user.id)
        self.assertIsNotNone(User.get(name='joao bolao'))
        self.assertIsNone(User.get(name=old_name))

    def test_user_to_dictionary(self):
        '''
        Test user to dictionary function
        '''
        user_dictionary = self.user.to_dict()
        self.assertEqual(len(user_dictionary), 3)
        self.assertIn('id', user_dictionary)
        self.assertIn('name', user_dictionary)
        self.assertIn('password', user_dictionary)
