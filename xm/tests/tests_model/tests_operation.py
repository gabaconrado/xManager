'''
    file: tests_operation.py
    author: gabriel conrado
    project: xManager
    brief:
        File with the functions for testing the operation models
'''

import unittest

from xm import database as db
from xm.model import Operation
from xm.exceptions import ValidationException
from xm.tests.utils import create_operation, create_account, create_user, \
    clean_users, clean_operations, clean_accounts


class OperationTests(unittest.TestCase):
    '''
    Tests for the Operation models
    '''

    @classmethod
    def setUpClass(cls):
        '''
        Setup class data
        '''
        super(OperationTests, cls).setUpClass()
        db.database_file = 'xmanager_test.db'
        db.create_database()
        cls.user = create_user()
        cls.source = create_account(cls.user, alias="Source")
        cls.destiny = create_account(cls.user, alias="Destiny")

    @classmethod
    def tearDownClass(cls):
        '''
        Clean the class data
        '''
        clean_accounts()
        clean_users()
        db.clean_database_file()
        super(OperationTests, cls).tearDownClass()

    def setUp(self):
        '''
        Setup test data
        '''
        super(OperationTests, self).setUp()
        self.operation = create_operation(
            source=self.source,
            destiny=self.destiny,
            amount=200.0,
            description="description"
            )

    def tearDown(self):
        '''
        Clean test data
        '''
        clean_operations()
        super(OperationTests, self).tearDown()

    def test_create_operation(self):
        '''
        Test the creation of operation model
        '''
        old_id = self.operation.id

        # Invalid operations
        self.operation.id = 0
        with self.assertRaises(ValidationException):
            self.operation.save()

        self.operation.id = '1234'
        with self.assertRaises(ValidationException):
            self.operation.save()

        self.operation.id = old_id
        self.operation.source = 'source'
        with self.assertRaises(ValidationException):
            self.operation.save()

        self.operation.source = self.source
        self.operation.destiny = 'destiny'
        with self.assertRaises(ValidationException):
            self.operation.save()

        self.operation.destiny = self.destiny
        self.operation.amount = '200.0'
        with self.assertRaises(ValidationException):
            self.operation.save()

        self.operation.amount = 200.0
        self.operation.description = 123
        with self.assertRaises(ValidationException):
            self.operation.save()

        self.operation.description = 'description'
        self.operation.date = 'date'
        with self.assertRaises(ValidationException):
            self.operation.save()

    def test_getting_operation(self):
        '''
        Test getting an operation from the db
        '''
        operations = Operation.get()
        self.assertIsNotNone(operations)
        operation_db = operations[0]
        self.assertEqual(self.operation.id, operation_db.id)
        self.assertEqual(self.operation.source.id, operation_db.source.id)
        self.assertEqual(self.operation.destiny.id, operation_db.destiny.id)
        self.assertEqual(self.operation.amount, operation_db.amount)
        self.assertEqual(self.operation.description, operation_db.description)
        self.assertEqual(self.operation.date, operation_db.date)

    def test_deleting_operation(self):
        '''
        Test deleting an operation from the db
        '''
        operations = Operation.get()
        self.assertIsNotNone(operations)

        self.operation.delete()

        operations = Operation.get()
        self.assertIsNone(operations)

    def test_updating_operation(self):
        '''
        Test updating an operation in the db
        '''
        old_id = self.operation.id
        old_amount = self.operation.amount

        self.operation.amount = 400.0
        self.operation.save()

        self.assertEqual(old_id, self.operation.id)
        self.assertIsNotNone(Operation.get(amount=400.0))
        self.assertIsNone(Operation.get(amount=old_amount))

    def test_operation_to_dictionary(self):
        '''
        Test operation to dictionary function
        '''
        operation_dictionary = self.operation.to_dict()
        self.assertEqual(len(operation_dictionary), 6)
        self.assertIn('id', operation_dictionary)
        self.assertIn('source', operation_dictionary)
        self.assertIn('destiny', operation_dictionary)
        self.assertIn('amount', operation_dictionary)
        self.assertIn('date', operation_dictionary)
        self.assertIn('description', operation_dictionary)
