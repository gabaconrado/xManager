#!/bin/bash

# pycodestyle
pycodestyle main.py
pycodestyle xm

# pylint
pylint main.py
pylint xm

# unit tests
python3 -m unittest -v xm.tests.tests_model.tests_user
python3 -m unittest -v xm.tests.tests_model.tests_operation
python3 -m unittest -v xm.tests.tests_model.tests_account
python3 -m unittest -v xm.tests.tests_model.tests_category
python3 -m unittest -v xm.tests.tests_model.tests_expense
python3 -m unittest -v xm.tests.tests_model.tests_dream
